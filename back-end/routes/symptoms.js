// Import required modules
const express = require('express');
const router = express.Router();
const mysql = require('mysql2');

// Create a MySQL connection pool
const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DATABASE
});

// Route to fetch symptoms
router.get('/symptoms', (req, res) => {
  // Query to select symptoms from the database
  const query = 'SELECT * FROM Symptom';

  // Execute the query
  pool.query(query, (err, results) => {
    if (err) {
      console.error('Error fetching symptoms:', err);
      return res.status(500).json({ error: 'Internal Server Error' });
    }

    // Return the list of symptoms as JSON
    res.json(results);
  });
});

module.exports = router;
