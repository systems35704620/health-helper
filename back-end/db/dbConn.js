const mysql = require('mysql2');

const conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS, 
    database: process.env.DB_DATABASE,
});

conn.connect((err) => {
    if(err){
        console.log("ERROR: " + err.message);
        return;    
    }
    console.log('Connection established');
});

let dataPool = {};

dataPool.allSymptoms = () => {
    return new Promise((resolve, reject) => {
        conn.query(`SELECT * FROM symptoms`, (err, res) => {
            if(err) { return reject(err); }
            return resolve(res);
        });
    });
};

dataPool.oneSymptom = (id) => {
    return new Promise((resolve, reject) => {
        conn.query(`SELECT * FROM symptoms WHERE id = ?`, id, (err, res) => {
            if(err) { return reject(err); }
            return resolve(res);
        });
    });
};

dataPool.createSymptom = (title, description) => {
    return new Promise((resolve, reject) => {
        conn.query(`INSERT INTO symptoms (title, description) VALUES (?, ?)`, [title, description], (err, res) => {
            if(err) { return reject(err); }
            return resolve(res);
        });
    });
};

dataPool.authUser = (username, password) => {
    return new Promise((resolve, reject) => {
        conn.query('SELECT * FROM User WHERE username = ?', username, (err, res, fields) => {
            if(err) { return reject(err); }
            return resolve(res);
        });
    });  
};

dataPool.addUser = (username, email, password, userType) => {
    return new Promise((resolve, reject) => {
        conn.query(`INSERT INTO User (username, email, password, userType) VALUES (?, ?, ?, ?)`, [username, email, password, userType], (err, res) => {
            if(err) { return reject(err); }
            return resolve(res);
        });
    });
};

module.exports = dataPool;